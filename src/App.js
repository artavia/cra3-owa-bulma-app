import React from 'react';

import { Alpha } from "./layout/alpha";

import { FormWeather } from './components/elements/form-weather/FormWeather';
import { FormForecast } from './components/elements/form-forecast/FormForecast';

const routes = [
  { path: '/weather', name: 'Weather', Component: FormWeather }
  , { path: '/forecast', name: 'Forecast', Component: FormForecast }
];

const App = () => {
  let element = (
    <>
      <Alpha routes={ routes } />
    </>
  );
  return element;
};

export { App };
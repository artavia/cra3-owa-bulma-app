const myLocations = [
  
  { number: 1, name: 'Pago Pago', id: 5881576 }
  , { number: 2, name: 'Adelaide', id: 2078025 }
  , { number: 3, name: 'Yap', id: 2081175 }
  , { number: 4, name: 'Sydney', id: 2147714 }
  , { number: 5, name: 'Noumea', id: 2139521 }
  , { number: 6, name: 'Wellington', id: 2179538 }
  , { number: 7, name: 'Polynésie Française', id: 4030656 }
  , { number: 8, name: 'Malé', id: 1282027 }
  , { number: 9, name: 'Delhi', id: 1273294 }
  , { number: 10, name: 'Dhaka', id: 1185241 }
  , { number: 11, name: 'Yangon', id: 1298822 }
  , { number: 12, name: 'Bangkok', id: 1609350 }
  , { number: 13, name: 'Hong Kong', id: 1819729 }
  , { number: 14, name: 'Singapore', id: 1880252 }
  , { number: 15, name: 'Seoul', id: 1835848 }
  , { number: 16, name: 'Tokyo', id: 1850147 }
  , { number: 17, name: 'Jeddah', id: 105343 }
  , { number: 18, name: 'Tehran', id: 112931 }
  , { number: 19, name: 'Dubai', id: 292223 }
  , { number: 20, name: 'Kabul', id: 1138958 }
  , { number: 21, name: 'Karachi', id: 1174872 }
  , { number: 22, name: 'Jerusalem', id: 281184 }
  , { number: 23, name: 'Cairo', id: 360630 }
  , { number: 24, name: 'Istanbul', id: 745044 }
  , { number: 25, name: 'Milan', id: 6542283 }
  , { number: 26, name: 'Roma', id: 3169070 }
  , { number: 27, name: 'Berlin', id: 2950159 }
  , { number: 28, name: 'Paris', id: 2988507 }
  , { number: 29, name: 'London', id: 2643743 }
  , { number: 30, name: 'Falls Church', id: 4758390 }
  , { number: 31, name: 'New York', id: 5128581 }
  , { number: 32, name: 'Brigus', id: 5908881 }
  , { number: 33, name: 'Eastport', id: 4963486 }
  , { number: 34, name: 'Miami', id: 4164138 }
  , { number: 35, name: 'Chicago', id: 4887398 }
  , { number: 36, name: 'Denver', id: 5419384 }
  , { number: 37, name: 'Bigfork', id: 5640284 }
  , { number: 38, name: 'Río de Janeiro', id: 3451190 }
  , { number: 39, name: 'Alajuela', id: 3624955 }
  , { number: 40, name: 'Guanacaste', id: 3623582 }
  , { number: 41, name: 'Cartago', id: 3624370 }
  , { number: 42, name: 'Turrialba', id: 3621184 }
  , { number: 43, name: 'Guapiles', id: 3623580 }
  , { number: 44, name: 'Liberia', id: 3623076 }
  , { number: 45, name: 'Limón', id: 3623064 }
  , { number: 46, name: 'Puntarenas', id: 3622228 }
  , { number: 47, name: 'Cañas', id: 3624468 }
  , { number: 48, name: 'San Isidro', id: 3621889 }
  , { number: 49, name: 'Chacarita', id: 3624288 }
  , { number: 50, name: 'Nicoya', id: 3622716 }
  , { number: 51, name: 'Heredia', id: 3623486 }
  , { number: 52, name: 'McKinney', id: 4710178 }
  , { number: 53, name: 'Leander', id: 4705708 }
  , { number: 54, name: 'Irving', id: 4700168 }
  , { number: 55, name: 'Las Vegas', id: 5506956 }
  , { number: 56, name: 'Mexico City', id: 3530597 }
  , { number: 57, name: 'Los Angeles', id: 5368361 }
  , { number: 58, name: 'Stockton', id: 5399020 }
  , { number: 59, name: 'San Diego', id: 5391811 }
  , { number: 60, name: 'San Francisco', id: 5391959 }
  , { number: 61, name: 'Vancouver', id: 6173331 }
  , { number: 62, name: 'Anchorage', id: 5879400 }
  , { number: 63, name: 'Honolulu', id: 5856195 }
  , { number: 64, name: 'Beijing', id: 1816670 }
  , { number: 65, name: 'Moscow', id: 524901 }
  , { number: 66, name: 'Damascus', id: 170654 }
  , { number: 67, name: 'Pyongyang', id: 1871859 }
  , { number: 68, name: 'Havana', id: 3553478 }
  , { number: 69, name: 'McMurdo Station', id: 6696480 }
  , { number: 70, name: 'Santiago', id: 3871336 }
]; 

// export default myLocations; // export class as Object
export {myLocations}; // export class as String
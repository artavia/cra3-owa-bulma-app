import React from 'react';

import { Nav } from '../components/elements/nav/Nav';
import { Footer } from '../components/elements/footer/Footer';

const Main = ( props ) => {

  // console.log( "MAIN.JS >>> props: ", props ); 

  const { routes } = props;
  
  let element = (
    <React.Fragment>
      <Nav routes={ routes } />
      { props.children }
      <Footer />
    </React.Fragment>
  ); 

  return element;
};

export {Main};

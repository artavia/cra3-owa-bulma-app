import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { CSSTransition } from 'react-transition-group';

import { Main } from './main';

import "../sass/animatepage.scss";

const Alpha = ( props ) => {

  // console.log( "ALPHA.JS >>> props: ", props ); 
  // RETURNS... Object { routes: (4) […] }

  const SwitchMe = (route,idx,arr) => {
    return RouteMe(route);
  };

  const RouteMe = (route) => {

    const { path, Component } = route;

    return (
      <Route key={path} exact path={path}>
        { ( {match} ) => {
          
          // console.log( "ALPHA.JS >>> match: ", match ); 

          return (
            <CSSTransition 
            in={ match.isExact === true }
            timeout={1406} 
            classNames="animatepage" 
            unmountOnExit
            appear
            >
              <div className="animatepage">
                <Component />
              </div>
            </CSSTransition>
          );

        } }
      </Route>
    );
  };

  let element = (
    <BrowserRouter>
      <Main routes={props.routes}>
        <Switch>
          
          { props.routes.map( SwitchMe ) }

          <Redirect to='/weather' />
        </Switch>
      </Main>
    </BrowserRouter>
  );
  return element;
};

export { Alpha };
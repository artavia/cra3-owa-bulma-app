import React from 'react';
import ReactDOM from 'react-dom';

import "./css/facss.css"; 

import "./js/vendors.js";
// import "./js/custommain.js";

import "./sass/bulmamine.scss";

import './sass/App.scss';

import "./sass/customstyles.scss";

import { App } from './App';

// import * as serviceWorker from './serviceWorker';
ReactDOM.render( <App />, document.getElementById('root') );

// serviceWorker.unregister();

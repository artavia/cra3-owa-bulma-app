import React from 'react';

import { BigLogo, SmallLogo } from '../elements/logo/Logo';

import { CustomError } from '../elements/custom-error/CustomError';
import { Loading } from '../elements/loading/Loading';

const Forecast = ( { state } ) => {
  
  // const mystate = state; console.log( `mystate: ${ JSON.stringify(mystate) }` ); 
  // const selectedvalue = state.selectedvalue; console.log( `selectedvalue: ${ selectedvalue }` ); 

  const singleDayStringer = ( unixtimestamp ) => { 
    let dateObj = new Date( (unixtimestamp * 1000) ); 
    let arr_days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']; 
    let day = arr_days[dateObj.getDay()]; 
    let timestring = `${day}`;
    return timestring;
  };

  
  const isError = state.isError;
  const isLoading = state.isLoading;

  const selectedweather = state.selectedweather; 
  // console.log( `selectedweather: ${ JSON.stringify( selectedweather ) }` ); 

  const cityname = state.cityname;
  const countryname = state.countryname; 
  const firstday = state.firstday; // console.log( 'firstday' , firstday );

  const selectedcitylist = state.selectedcitylist; // console.log( 'selectedcitylist' , selectedcitylist );

  if( isLoading ){
    return(
      <Loading />
    );
  }

  if( isError ){ 
    
    // console.log( "if isError... : " , isError );
    // console.log( "if isError... state: " , state );

    return(
      <CustomError error={ isError }></CustomError>
    );
  }

  if( !selectedweather ){
    return(
      <BigLogo />
    );
  }

  let element = (
      
    <section className="section is-info" id="sectionFrag">
      <div className="container">
        
        <div className="tile is-ancestor">

          <div className="tile is-vertical is-8">
            <div className="tile">
              <div className="tile is-parent is-vertical">
                
                <article className="tile is-child notification is-primary">
                  
                  <SmallLogo />

                  <p className="title">{ cityname } Weather Forecasts</p>
                  <p className="subtitle">Nearest weather station: { cityname }, { countryname } &nbsp;</p>
                </article>

                { firstday.map(
                  (day) => <article key={day.dt} className="tile is-child notification is-warning">
                    <p className="title is-4">Current Weather in { cityname }</p>
                    <p className="subtitle is-3">{ Math.round( day.main.temp ) }&deg; F </p>
                    <p className="subtitle is-5">High: { Math.round( day.main.temp_max ) }&deg; F </p>
                    <p className="subtitle is-5">Low: { Math.round( day.main.temp_min ) }&deg; F </p>
                    <p className="subtitle is-4"> { day.weather[0].main } </p>
                  </article>
                ) }

              </div>

              <div className="tile is-parent">
                <article className="tile is-child notification is-info">
                  
                  { firstday.map(
                    (day) => <figure key={day.dt} className="image">
                      <img src={ `/images/weathercodes/${ day.weather[0].id }.gif` } alt="weathercode" />
                    </figure>
                  ) }

                </article>
              </div>
              
            </div>
            <div className="tile is-parent">
              <article className="tile is-child notification is-success">
                <div className="content">
                  <p className="title">Detailed Local Forecast for { cityname }</p>

                  <div className="content">
                    
                    { selectedcitylist.map(
                      (day) => <p key={day.dt}>
                        
                        <strong>{ singleDayStringer( day.dt ) }. </strong>&nbsp;{ `${ day.weather[0].description.charAt(0).toUpperCase() }${day.weather[0].description.substr(1)}` }.&nbsp;High { Math.round( day.main.temp_max ) } &deg; F.&nbsp;Low { Math.round( day.main.temp_min ) } &deg; F.&nbsp;Winds up to { Math.round( day.wind.speed ) } mph in some cases.

                      </p>
                    ) }
                    
                  </div>

                </div>
              </article>
            </div>
          </div>
          
      
          <div className="tile is-parent">
            <article className="tile is-child notification is-danger">
              
              { firstday.map(
                (day) => <div key={day.dt} className="content">
                  <p className="content"><em>Forecasted weather for { day.dt_txt }</em></p>
                  <dl>
                    <dt>Wind: </dt>
                    <dd>Winds at  { Math.round( day.wind.speed ) } mph</dd>
                    <dt>Humidity: </dt>
                    <dd>{ day.main.humidity } &#37;</dd>
                    <dt>Barometric Pressure: </dt>
                    <dd>{ Math.round( day.main.pressure ) } hPa</dd>
                  </dl>
                </div>
              ) }

            </article>
          </div>
      
        </div>

        <div className="tile is-ancestor">
          <div className="tile is-parent">
            
            { selectedcitylist.map(
              (day) => <article key={day.dt} className="tile is-child notification is-black">
                <p className="title has-text-centered is-4">{ singleDayStringer( day.dt ) }</p>
                <p className="subtitle has-text-centered"> { day.weather[0].description } </p>
                <figure className="image is-128x128 customCenteredMargin">
                  <img className="img customImgBackdrop" src={ `/images/weathercodes/${ day.weather[0].id }.gif` } alt="" />
                </figure>
                <p className="has-text-centered"><strong>High: </strong> { Math.round( day.main.temp_max ) } &deg; F.</p>
                <p className="has-text-centered"><strong>Low: </strong> { Math.round( day.main.temp_min ) } &deg; F.</p>
              </article> 
            ) }

          </div>
        </div>

      </div>
    </section>
    
  );

  return element;

};

export { Forecast };
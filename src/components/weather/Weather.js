import React from 'react';

import { BigLogo, SmallLogo } from '../elements/logo/Logo';

import { CustomError } from '../elements/custom-error/CustomError';
import { Loading } from '../elements/loading/Loading';

const Weather = ( { state } ) => {
  
  // const mystate = state; console.log( `mystate: ${ JSON.stringify(mystate) }` ); 
  // const selectedvalue = state.selectedvalue; console.log( `selectedvalue: ${ selectedvalue }` ); 
  
  const singleDayStringer = ( unixtimestamp ) => { 
    let nowObj = new Date();
    let tzoffset = -( nowObj.getTimezoneOffset() ); 
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; 
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let arr_days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']; 
    let day = arr_days[dateObj.getUTCDay()]; 
    let timestring = `${day}`;
    return timestring;
  };
  
  const dateStringer = ( unixtimestamp ) => { 
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let arr_months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    let month = arr_months[dateObj.getUTCMonth()]; 
    let date = dateObj.getUTCDate(); 
    let year = dateObj.getUTCFullYear(); 
    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 
    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${month} ${date}, ${year} at ${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return timestring;
  };
  
  const sunriseStringer = ( unixtimestamp ) => {
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 
    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return timestring;
  };
  
  const sunsetStringer = ( unixtimestamp ) => {
    let nowObj = new Date();
    // getTimezoneOffset() returns the time difference between UTC time and local time in minutes.
    let tzoffset = -( nowObj.getTimezoneOffset() ); // -360 (in minutes) eq. to -6 hours from GMT
    // this returns getTimezoneOffset in seconds (* 60 ), then, in milliseconds (* 1000 )
    let timezoneoffset_milliseconds = tzoffset * 60 * 1000 ; // -21600000 milliseconds
    let dateObj = new Date( (unixtimestamp * 1000) + timezoneoffset_milliseconds ); 
    let hours = dateObj.getUTCHours(); 
    let minutes = dateObj.getUTCMinutes(); 
    let seconds = dateObj.getUTCSeconds(); 
    let amPm = hours >= 12 ? 'pm' : 'am'; 
    let hourstostring = ( "0" + ( ( hours % 12 ) || 12 ) ).substr(-2);
    let minutestostring = ("0" + minutes).substr(-2);
    let secondstostring = ("0" + seconds).substr(-2);    
    let timestring = `${hourstostring}:${minutestostring}:${secondstostring} ${amPm}`;
    return timestring;
  };
  

  const isError = state.isError;
  const isLoading = state.isLoading;

  const selectedweather = state.selectedweather; 
  // console.log( `selectedweather: ${ JSON.stringify( selectedweather ) }` ); 

  const cityname = state.cityname;
  const countryname = state.countryname;
  const weatheridcode = `/images/weathercodes/${ state.weatheridcode }.gif`; 
  const currenttemp = Math.round( state.currenttemp );
  const hightemp = Math.round( state.hightemp );
  const lowtemp = Math.round( state.lowtemp );
  const weathermain = state.weathermain;
  const currentdate = dateStringer( state.currentdate );
  const windspeed = Math.round( state.windspeed );
  const visibility = state.visibility;
  const humidity = state.humidity;
  const barometricpressure = state.barometricpressure;
  const timesunrise = sunriseStringer( state.timesunrise );
  const timesunset = sunsetStringer( state.timesunset );
  const weekday = singleDayStringer( state.weekday );
  const weatherdescription = `${ state.weatherdescription.charAt(0).toUpperCase() }${state.weatherdescription.substr(1)}`;

  if( isLoading ){
    return(
      <Loading />
    );
  }

  if( isError ){ 
    
    // console.log( "if isError... : " , isError );
    // console.log( "if isError... state: " , state );

    return(
      <CustomError error={ isError }></CustomError>
    );
  }

  if( !selectedweather ){
    return(
      <BigLogo />
    );
  }

  let element = (
    
    <section className="section is-info" id="sectionFrag">
                  
      <div className="container">
        <div className="tile is-ancestor">
          
          <div className="tile is-vertical is-8">
            <div className="tile">
              
              <div className="tile is-parent is-vertical">
                <article className="tile is-child notification is-primary">
                  
                  <SmallLogo />

                  <p className="title">{ cityname } Weather Forecasts</p>
                  <p className="subtitle">Nearest weather station: { cityname }, { countryname } &nbsp;</p>
                </article>

                <article className="tile is-child notification is-warning">
                  <p className="title is-4">Current Weather in { cityname }</p>
                  <p className="subtitle is-3">{ currenttemp }&deg; F</p>
                  <p className="subtitle is-5">High: { hightemp }&deg; F</p>
                  <p className="subtitle is-5">Low: { lowtemp }&deg; F</p>
                  <p className="subtitle is-4">{ weathermain }</p>
                </article>

              </div>
              
              <div className="tile is-parent">
                <article className="tile is-child notification is-info">
                  
                  <figure className="image">
                    <img src={ weatheridcode } alt="weathercode" />
                  </figure>
                  
                  <p className="content"><em>Current conditions as of { currentdate }</em></p>

                </article>
              </div>
              
            </div>
            <div className="tile is-parent">
              <article className="tile is-child notification is-success">
                <div className="content">
                  <p className="title">Detailed Local Forecast for { cityname }</p>
                  <div className="content">
                    <p><strong>{ weekday }. </strong>&nbsp;{ weatherdescription }.&nbsp;High { hightemp } &deg; F.&nbsp;Low { lowtemp } &deg; F.&nbsp;Winds up to { windspeed } mph in some cases. </p>
                  </div>
                </div>
              </article>
            </div>
          </div>
          
          <div className="tile is-parent">
            <article className="tile is-child notification is-danger">
              <div className="content">
                <dl>
                  <dt>Wind: </dt>
                  <dd>Winds at { windspeed } mph</dd>

                  <dt>Visibility: </dt>
                  <dd>{ visibility } meters</dd>

                  <dt>Humidity: </dt>
                  <dd>{ humidity } &#37;</dd>

                  <dt>Barometric Pressure: </dt>
                  <dd>{ barometricpressure } hPa</dd>

                  <dt>Sunrise (local time): </dt>
                  <dd>{ timesunrise }</dd>

                  <dt>Sunset (local time): </dt>
                  <dd>{ timesunset }</dd>
                </dl>
              </div>
            </article>
          </div>

        </div>
      </div>

    </section>
  );
  
  return element;

};

export { Weather };
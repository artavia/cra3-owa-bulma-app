import React from 'react';
import { NavLink } from 'react-router-dom';

import bulmaLogoUrl from '../../../assets/layout/bulma-logo.png';

const Nav = ( props ) => {
  
  const { routes } = props;
  // console.log( "nav.js >>> routes: " , routes );

  const NavLinkMe = ( route, idx, arr ) => {
    
    return (
      <NavLink key={ route.path } className="navbar-item" to={ route.path } activeClassName={ 'is-active' } exact>
        {route.name}
      </NavLink>
    );
  };

  const noOpLink = (event) => { event.preventDefault(); };
  
  const flexExtendBurger = (event) => {

    if( event !== null && event.type === 'click' ){
      let burger = event.target; // console.log( 'burger: ' , burger );
      let attrTarget = burger.dataset.target; // console.log( "attrTarget: " , attrTarget );
      let $targetEl = document.getElementById(attrTarget); // console.log( "$targetEl: " , $targetEl );

      burger.classList.toggle('is-active');
      
      if( $targetEl !== null ){
        $targetEl.classList.toggle('is-active'); 
      }
      
    } 

  };

  let newDate = new Date();
  let myPresentYear = newDate.getFullYear();  
  
  let element = (
    <section className="section hero is-bold is-warning">
      <div className="hero-body container has-text-centered">
        <h1 id="apptitle" className="title is-1">Open Weather App</h1>
        <h2 className="subtitle is-2">CRA3 OWA Bulma App</h2>
        <h3 className="subtitle is-3">
          Application modernized for use in { myPresentYear }
        </h3>
      </div>
      <nav className="navbar is-transparent">

        <div className="navbar-brand">
          <div className="navbar-burger burger" data-target="navbarExampleTransparentExample" onClick={ flexExtendBurger }>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      
        <div id="navbarExampleTransparentExample" className="navbar-menu">

          <div className="navbar-start">

            { routes.map( NavLinkMe ) }

          </div>
          
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="field is-grouped">
                <p className="control has-text-centered">
                  <NavLink className="navbar-item" to={ '//artavia.gitlab.io' } target="_blank" onClick={ noOpLink } activeClassName={ 'is-active' } >
                    Made with &nbsp;<img src={ bulmaLogoUrl } alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28" />
                  </NavLink>
                </p>
              </div>
            </div>
          </div>

        </div>

      </nav>
    </section>
  );

  return element;
};

export { Nav };
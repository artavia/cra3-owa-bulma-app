import React from 'react';

import logoUrl from '../../../assets/layout/owa_logo.gif';

const BigLogo = () => {
  
  let element = (
    <figure id="fFrag" className="image">
      <img id="initialIcon" className="customStrongImage" src={ logoUrl } alt="owa logo" />
    </figure>
  );

  return element;
};

const SmallLogo = () => {
  
  let element = (
    <figure className="image">
      <img id="strongImage" src={ logoUrl } alt="owa logo" />
    </figure>
  );

  return element;
};

export { BigLogo, SmallLogo };
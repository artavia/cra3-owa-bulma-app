import React , {
  useState
 } from 'react';

import { Forecast } from '../../forecast/Forecast';
import { myLocations } from '../../../models/DataModel';
import { OwaApiKey } from '../../../models/OwaApiKey';

import { SharedForm } from '../shared-form/SharedForm';

const FormForecast = () => {

  let initialObject = {
      
    isError: null 
    , isLoading: false 
    
    , locations: myLocations 
    , selectedvalue: "" 
    , selectedweather: null 
    
    , cityname: '' 
    , countryname: '' 
    , firstday: null 
    , selectedcitylist: null
  };

  const [ state , setState ] = useState( initialObject );

  
  // #################### BEGINNING ##################
  const onForecastClick = (event) => {
    // console.log( "Click has taken place" );
    // console.log( "event: ", event );

    fetchCoordinatesWrapper();
    event.preventDefault();
  };

  const fetchCoordinatesWrapper = () => {
    // console.log( "   >>>  >>>    fetchCoordinatesWrapper " );
    
    /**/ locationPromise()
    .then((response)=>{  
      // console.log( ".then() response()", response() ); // typeof() returns object
      // return response.json(); // NEITHER A fetch call nor is it necessary
      return response();
    })
    .then( (coordinates) => {
      // console.log( '.then() coordinates' , coordinates );
      
      setState(
        { 
          ...state 
          , isLoading: true 
        }
      );

      runGeoForecast( coordinates );

    } )
    .catch( (error) => {
      
      setState( 
        { 
          ...state 
          , isLoading: false 
          , isError: error 
        } 
      );

      // console.log( "fetchCoordinatesWrapper() .catch() error: " , error );
    } ); /**/

  };

  const locationPromise = () => {
    // console.log( "   >>>  >>>    locationPromise " );

    /**/ return new Promise( function(resolveHandler,rejectHandler){

      navigator.geolocation.getCurrentPosition( 
        
        function success(pos) {

          resolveHandler( () => {
            var crdobj = {lat: pos.coords.latitude , lon: pos.coords.longitude };  
            // console.log("geolo success resolveHandler... crdobj" , crdobj );

            return crdobj;
          } );

          rejectHandler( (error) => {
            console.log("geolo success rejectHandler... error" , error );
          } );
        }
        , function error(error) {
          
          // console.warn(`ERROR(${error.code}): ${error.message} , ${error} `); 
          // console.log("geolo error... error" , error );

          setState(
            { 
              ...state 
              , isLoading: false 
              , isError: error 
            }
          );

        } 
        , {
          enableHighAccuracy: true
          , timeout: 10000
          , maximumAge: 0
        } 
      );

    }); /**/

  };

  const runGeoForecast = (coord) => {
    // console.log( "   >>>  >>>    runGeoForecast  coord PM: " , coord );

    /**/ const baseUrl = `https://api.openweathermap.org`;
    const forecastpath = `/data/2.5/forecast`; 
    const appId = OwaApiKey;
    const unitTemp = `imperial`; // e.g. -- metric, imperial
    const linguafranca = `en`; // e.g. -- en, es, ja
    const query = `appid=${appId}&units=${unitTemp}&lang=${linguafranca}`;
    
    fetch(`${baseUrl}${forecastpath}?lat=${coord.lat}&lon=${coord.lon}&${query}`)
    .then((response)=>{ 
      // console.log("owapi response" , response);
      return response.json();
    })
    .then((data)=>{ 
      
      // console.log("owapi then json data" , data );

      let listoffive = data.list.filter( function(el) {
        return ( el.dt_txt.includes( '12:00:00' ) ); // for normal browsers
        // return ( el.dt_txt.indexOf( '12:00:00' ) !== -1 ); // for benefit of IE11 and less
      } ); 

      const firstday = listoffive.filter(
        function( el, idx, arr ){
          return ( idx === 0 );
        } 
      );

      
      setState ({
        ...state 
        , isLoading: false 
        , selectedweather: data
        , firstday: firstday 
        , selectedcitylist: listoffive 
        , cityname: data.city.name 
        , countryname: data.city.country 
      });

    })
    .catch( (error) => {
      setState( { ...state , isLoading: false , isError: error } );
    } ); /**/

  };

  // ################# IN PROGRESS ###############
  const onForecastChange = async (event) => { 
    // NEW HAT
    // setState( { ...state , PROPKEY: PROPVAL } );

    event.persist();

    if( (await event.target.value === '') ){
      // console.log( "You DID NOT choose a city");
      
      setState( {  
        ...state 
        , selectedvalue: await event.target.value 

        , isError: await null 
        , isLoading: await false 

        , selectedweather: await null     
        , cityname: await '' 
        , countryname: await '' 
        , firstday: await null 
        , selectedcitylist: await null

      } );

    }
    else    
    if( (await event.target.value !== '') ){
      // console.log( "You chose a CITY");

      setState( { 
        ...state 
        , selectedvalue: await event.target.value 
        , isLoading: await true 
      } );
      
      // build out the ajax call/response, then, hash out final props
      /**/
      const baseUrl = `https://api.openweathermap.org`;
      const forecastpath = `/data/2.5/forecast`; 
      const appId = OwaApiKey;
      const unitTemp = `imperial`; // e.g. -- metric, imperial
      const linguafranca = `en`; // e.g. -- en, es, ja
      const query = `appid=${appId}&units=${unitTemp}&lang=${linguafranca}`;
      
      /**/
      let id = await event.target.value; // console.log( "id: " , id );
      
      fetch(`${baseUrl}${forecastpath}?id=${id}&${query}`)
      .then((response)=>{ 
        // console.log("owapi response" , response);
        return response.json();
      })
      .then((data)=>{ 
        
        // console.log("owapi then json data" , data );

        let listoffive = data.list.filter( function(el) {
          return ( el.dt_txt.includes( '12:00:00' ) ); // for normal browsers
          // return ( el.dt_txt.indexOf( '12:00:00' ) !== -1 ); // for benefit of IE11 and less
        } ); 

        const firstday = listoffive.filter(
          function( el, idx, arr ){
            return ( idx === 0 );
          } 
        );

        setState ({ 
          ...state 
          
          , isLoading: false 
          , isError: null

          , selectedweather: data 
          , firstday: firstday 
          , selectedcitylist: listoffive 
          , cityname: data.city.name 
          , countryname: data.city.country 
        });

      })
      .catch( (error) => {
        setState( { ...state , isLoading: false , isError: error } );
      } ); /**/

    }
  };

  // ################# END IN PROGRESS ###############

  // #################### THE END #################### 
  let element = (
      
    <section className="section is-medium">
      <div className="container is-fluid">
        
        <div className="block">
          <div className="columns box">
            <div className="column" id="disburser">
              
              <SharedForm 
                state={ state } 
                scopelabel={"forecast"} 
                selectPropForecast={ onForecastChange } 
                clickPropForecast={ onForecastClick }
              >
              </SharedForm>

            </div>
          </div>
        </div>

        <div className="block">
          <div className="columns box">
            <div className="column" id="sourceOut">

              <Forecast state={ state }  />

            </div>
          </div>
        </div>

      </div>
    </section>
    
  );

  return element;
};

export { FormForecast };
// class FormForecast extends React.Component
// export { FormForecast };
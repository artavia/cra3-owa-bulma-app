import React from 'react';

const Footer = () => {
  
  let newDate = new Date();
  let myPresentYear = newDate.getFullYear(); 

  let element = (
    <section className="section">
      <div className="container">
        <footer className="footer">
          <div className="has-text-centered">
            <p className="content is-small">
              Concept, artwork, etc. by  &lsquo;don Lucho&rsquo; (yada, yada, yada) &copy; { myPresentYear }
            </p>
          </div>
        </footer>
      </div>
    </section>
  );
  return element;
};

export { Footer };
import React , {
  useState
 } from 'react';

import { Weather } from '../../weather/Weather';
import { myLocations } from '../../../models/DataModel';
import { OwaApiKey } from '../../../models/OwaApiKey';

import { SharedForm } from '../shared-form/SharedForm';

const FormWeather = () => {

  let initialObject = {
      
    isError: null 
    , isLoading: false 

    , locations: myLocations 
    , selectedvalue: "" 
    , selectedweather: null 

    , cityname: '' 
    , countryname: '' 
    , weatheridcode: '' 
    , currenttemp: '' 
    , hightemp: '' 
    , lowtemp: '' 
    , weathermain: '' 
    , currentdate: '' 
    , windspeed: '' 
    , visibility: '' 
    , humidity: '' 
    , barometricpressure: '' 
    , timesunrise: ''
    , timesunset: '' 
    , weekday: '' 
    , weatherdescription: '' 
  };
  
  const [ state , setState ] = useState( initialObject );
  
  // #################### BEGINNING ##################
  const onWeatherClick = ( event ) => {
    // console.log( "Click has taken place" );
    // console.log( "event: ", event );

    fetchCoordinatesWrapper();
    event.preventDefault();
  };

  const fetchCoordinatesWrapper = () => {
    // console.log( "   >>>  >>>    fetchCoordinatesWrapper " );

    /**/ locationPromise()
    .then((response)=>{  
      // console.log( ".then() response()", response() ); // typeof() returns object
      // return response.json(); // NEITHER A fetch call nor is it necessary
      return response();
    })
    .then( (coordinates) => {
      // console.log( '.then() coordinates' , coordinates );
      
      setState( { ...state , isLoading: true } );

      runGeoWeather( coordinates );

    } )
    .catch( (error) => {

      setState( { ...state , isLoading: false , isError: error } );

      // console.log( "fetchCoordinatesWrapper() .catch() error: " , error );
    } ); /**/

  };

  const locationPromise = () => {
    // console.log( "   >>>  >>>    locationPromise " );

    /**/ return new Promise( function(resolveHandler,rejectHandler){

      navigator.geolocation.getCurrentPosition( 
        
        function success(pos) {

          resolveHandler( () => {
            var crdobj = {lat: pos.coords.latitude , lon: pos.coords.longitude }; 
            // console.log("geolo success resolveHandler... crdobj" , crdobj );

            return crdobj;
          } );

          rejectHandler( (error) => { 
            console.log("geolo success rejectHandler... error" , error );
          } );
        }
        , function error(error) {

          // console.warn(`ERROR(${error.code}): ${error.message} , ${error} `); 
          // console.log("geolo error... error" , error );

          setState( { ...state , isLoading: false , isError: error } );

        } 
        , {
          enableHighAccuracy: true
          , timeout: 10000
          , maximumAge: 0
        } 
      );

    });  /**/

  };

  const runGeoWeather = (coord) => {
    // console.log( "   >>>  >>>    runGeoWeather  coord PM " , coord );

    /**/ const baseUrl = `https://api.openweathermap.org`;
    const weatherpath = `/data/2.5/weather`;
    const appId = OwaApiKey; 
    const unitTemp = `imperial`; // e.g. -- metric, imperial
    const linguafranca = `en`; // e.g. -- en, es, ja
    const query = `appid=${appId}&units=${unitTemp}&lang=${linguafranca}`;
    
    fetch(`${baseUrl}${weatherpath}?lat=${coord.lat}&lon=${coord.lon}&${query}`)
    .then((response)=>{ 
      // console.log("owapi response" , response);
      return response.json();
    })
    .then((data)=>{

      // console.log("owapi then json data" , data );

      setState ( { ...state , isLoading: false , selectedweather: data
        , cityname: data.name 
        , countryname: data.sys.country 
        , weatheridcode: data.weather[0].id 
        , currenttemp: data.main.temp 
        , hightemp: data.main.temp_max 
        , lowtemp: data.main.temp_min 
        , weathermain: data.weather[0].main 
        , currentdate: data.dt 
        , windspeed: data.wind.speed 
        , visibility: data.visibility 
        , humidity: data.main.humidity 
        , barometricpressure: data.main.pressure 
        , timesunrise: data.sys.sunrise 
        , timesunset: data.sys.sunset 
        , weekday: data.dt 
        , weatherdescription: data.weather[0].description 
      });

    })
    .catch( (error) => {
      setState( { ...state , isLoading: false , isError: error } );
    } ); /**/

  };

  // ################# IN PROGRESS ###############
  const onWeatherChange = async (event) => { 
    // NEW HAT
    // setState( { ...state , PROPKEY: PROPVAL } );
    
    event.persist();
    
    if( (await event.target.value === '') ){
      // console.log( "You DID NOT choose a city");
      
      setState( { 
        ...state 
        , selectedvalue: await event.target.value 
        
        , isError: await null 
        , isLoading: await false 
        
        , selectedweather: await null
        , cityname: await '' 
        , countryname: await '' 
        , weatheridcode: await '' 
        , currenttemp: await '' 
        , hightemp: await '' 
        , lowtemp: await '' 
        , weathermain: await '' 
        , currentdate: await '' 
        , windspeed: await '' 
        , visibility: await '' 
        , humidity: await '' 
        , barometricpressure: await '' 
        , timesunrise: await ''
        , timesunset: await '' 
        , weekday: await '' 
        , weatherdescription: await '' 
      } );
    }
    else
    if( (await event.target.value !== '') ){
      // console.log( "You chose a CITY");
      
      setState( { 
        ...state
        , selectedvalue: await event.target.value 

        , isLoading: await true 
      } );
      
      // build out the ajax call/response, then, hash out final props
      /**/
      const baseUrl = `https://api.openweathermap.org`;
      const weatherpath = `/data/2.5/weather`;
      const appId = OwaApiKey; 
      const unitTemp = `imperial`; // e.g. -- metric, imperial
      const linguafranca = `en`; // e.g. -- en, es, ja
      const query = `appid=${appId}&units=${unitTemp}&lang=${linguafranca}`;
      // const dracula = async () => {};
      
      /**/
      let id = await event.target.value; // console.log( "id: " , id );

      fetch(`${baseUrl}${weatherpath}?id=${id}&${query}`)
      .then((response)=>{ 
        // console.log("owapi response" , response);
        return response.json();
      })
      .then((data)=>{

        // console.log("owapi then json data" , data );
        
        setState ({
          ...state 
          
          , isLoading: false 
          , isError: null
          
          , selectedweather: data
          , cityname: data.name 
          , countryname: data.sys.country 
          , weatheridcode: data.weather[0].id 
          , currenttemp: data.main.temp 
          , hightemp: data.main.temp_max 
          , lowtemp: data.main.temp_min 
          , weathermain: data.weather[0].main 
          , currentdate: data.dt 
          , windspeed: data.wind.speed 
          , visibility: data.visibility 
          , humidity: data.main.humidity 
          , barometricpressure: data.main.pressure 
          , timesunrise: data.sys.sunrise 
          , timesunset: data.sys.sunset 
          , weekday: data.dt 
          , weatherdescription: data.weather[0].description 
        });

      })
      .catch( (error) => {
        setState( { ...state , isLoading: false , isError: error } );
      } ); /**/

    } 

  };

  // ################# END IN PROGRESS ###############

  // #################### THE END #################### 
  let element = (

    <section className="section is-medium">
      <div className="container is-fluid">

        <div className="block">
          <div className="columns box">
            <div className="column" id="disburser">
              
              <SharedForm 
                state={ state } 
                scopelabel={"weather"} 
                selectPropWeather={ onWeatherChange } 
                clickPropWeather={ onWeatherClick }
              >
              </SharedForm>

            </div>
          </div>
        </div>
        
        <div className="block">
          <div className="columns box">
            <div className="column" id="sourceOut">
              
              <Weather state={ state } />

            </div>
          </div>
        </div>
      </div>
    </section>
  
  );

  return element;

};

export { FormWeather };
// class FormWeather extends React.Component
// export { FormWeather };
import React from 'react';

const OptionItem = ( {location} ) => {
  let valstring = `${location.id}`;
  return(
    <option value={valstring}> {location.name} </option>
  );
};

export { OptionItem };
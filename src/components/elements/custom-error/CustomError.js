import React from 'react';

// GEOLO 
// console.warn( `ERROR(${error.code}): ${error.message} , ${error} ` );
/* ERROR(1): User denied geolocation prompt , [object PositionError] 
ERROR(2): Unknown error acquiring position , [object PositionError]  */

const CustomError = ( error ) => {

  // GENERIC
  //// console.log( "error: " , error ); // Error: "Network Error"
  // console.log( "typeof( error.error ): " , typeof( error.error ) ); // Error: "Network Error"

  /**/ 
  if (error.response) {
    // The request was made and the server responded with a status code that falls out of the range of 2xx
    // console.log( "error.response.data" , error.response.data );
    // console.log( "error.response.status" , error.response.status );
    // console.log( "error.response.headers" , error.response.headers );
  } 
  else 
  if (error.request) {
    // The request was made but no response was received 
    // `error.request` is an instance of [ XMLHttpRequest ] in the browser and an instance of [ http.ClientRequest ] in node.js
    console.log( "error.request" , error.request );
  }
  if (error.error) {
    //// console.log( 'error.error.message ', error.error.message );
  }
  else {
    // Something happened in setting up the request that triggered an Error
  } 
  
  /**/

  let element = (
    <section className="hero is-danger">
      <div className="hero-body">
        <div className="container has-text-centered">
          
          <h3 className="title is-3">
            Loading Error
          </h3>

          <h4 className="subtitle is-4">
            An error has occurred
          </h4>

          {/* <code>Error: { error } </code> */}

          { ( error.error ) && <code>Error: { error.error.message } </code> }

        </div>
      </div>
    </section>
  );
  
  return element;
};

export { CustomError };
import React from 'react';

import { OptionItem } from '../option-item/OptionItem';

const SharedForm = ( props ) => {

  // console.log( `SharedForm >>> props: `, props );
  // console.log( `SharedForm >>> props: ${ JSON.stringify( props ) }` ); 
  
  // console.log( `SharedForm >>> props.scopelabel: `, props.scopelabel ); 

  // console.log( `SharedForm >>> props.state: `, props.state ); 
  // console.log( `SharedForm >>> props.state.selectedvalue: `, props.state.selectedvalue ); 
  // console.log( `SharedForm >>> props.state.locations: `, props.state.locations ); 

  /**/ const options = props.state.locations.map(
    ( location ) => {
      return(
        <OptionItem key={ location.number.toString() } location={location} />
      );
    }
  ); /**/
  
  let element = (
    <form action="" id="forecaster">
      
      <fieldset className="customFold">

        <legend className="customLofty">find { props.scopelabel }</legend>
        
        <p className="content is-small customParaphrase">Choose any name from the list of more than 70 cities so you can obtain weather information for that location. </p>
        
        <div className="field">
          
          <label htmlFor="WhenTheLightsGoDownInThe" className="label">City</label>
          
          <div className="control has-icons-left">
            <div className="select is-success">
              
              { props.scopelabel === 'weather' && (

              <select name="WhenTheLightsGoDownInThe" id="WhenTheLightsGoDownInThe" onChange={ props.selectPropWeather } selectedvalue={ props.state.selectedvalue }>
                  
                <option value="" defaultValue="">Choose a location</option>
                { options }

              </select>

              )}


              { props.scopelabel === 'forecast' && (

              <select name="WhenTheLightsGoDownInThe" id="WhenTheLightsGoDownInThe" onChange={ props.selectPropForecast } selectedvalue={ props.state.selectedvalue }>
                  
                <option value="" defaultValue="">Choose a location</option>
                { options }

              </select>
              
              )}

            </div>
            <span className="icon is-large is-left">
              <i className="fas fa-globe"></i>
            </span>
          </div>

        </div>

      </fieldset>

      <fieldset className="customFold">
        
        <legend className="customLofty">find with global positioning</legend>
        
        <p className="content is-small customParaphrase">Obtain weather information by means of your latitude and longitude. </p>

        <div className="field">
          <label htmlFor="geolo" className="label">Geolocation positioning</label>
          <div className="control">

            
            { props.scopelabel === 'weather' && (
            <button id="geolo" className="button is-success is-outlined customCaps" onClick={ props.clickPropWeather } >
              Get {props.scopelabel}
            </button>
            )}
            
            { props.scopelabel === 'forecast' && (
            <button id="geolo" className="button is-success is-outlined customCaps" onClick={ props.clickPropForecast } >
              Get {props.scopelabel}
            </button>
            )}
            

          </div>
        </div>

      </fieldset>

    </form>
  );
  
  return element;
};

export { SharedForm };
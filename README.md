# Description
Fun with CRA v3 hooks, Bulma and OpenWeatherMap.

## Building on previous precedent.
Other than putting out to pasture some really old css, Bulma has been brought in to rescue the project. Plus, it was built with Create React App version 3. I fleshed out a "shared" form, too. 

## What is presently implemented
I have added a custom error element and a loading element. With the former, there were many opportunities for improvement to handle loading errors. With the latter, the usability factor is improved exponentially for a more complete user experience. Finally, customized routing animations and page transitions were added to said project.

## Please visit new project today
You can see [the lesson at surge.sh](https://cra3-owa-bulma-app.surge.sh "the lesson at surge") and decide if this is something for you to play around with at a later date.

## There is an available companion project
You can see the [original repository](https://gitlab.com/artavia/ng8-owa-bulma-app "the lesson at gitlab"), then, decide if that is something for you to play around with, too.

## My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!